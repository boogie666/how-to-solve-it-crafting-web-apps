(ns todomvc.data
  (:require
    [cljs.test :refer-macros [deftest is testing use-fixtures]]
    [todomvc.model :refer [ADD EDIT LIST]]
    ))

(def initial-model {:context {:status LIST}
                     :todos   {}})
(def a-todo {:id 1
             :text "todo"
             :done? true
             :time 123})
(def a-second-todo {:id 2
             :text "todo 2"
             :done? false
             :time 124})

(def one-todo-list-mode-no-filter {:context {:status LIST}
                         :todos {:1 a-todo}})

(def one-todo-list-mode {:context {:status LIST
                                   :filter true}
                         :todos {:1 a-todo}})

(def one-todo-edit-mode (-> one-todo-list-mode
                            (assoc-in [:context :status] EDIT)
                            (assoc-in [:context :selected-id] 1)
                            ))

(def two-todos-list-mode {:context {:status LIST
                                   :filter true}
                         :todos {:1 a-todo
                                 :2 a-second-todo}})

(def two-todo-list-mode-no-filter {:context {:status LIST}
                                   :todos {:1 a-todo
                                           :2 a-second-todo}})
