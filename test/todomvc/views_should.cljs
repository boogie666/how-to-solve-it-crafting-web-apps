(ns todomvc.views-should
  (:require
    [cljs.test :refer-macros [deftest is testing use-fixtures]]
    [cljsjs.enzyme]
    [todomvc.controller :as controller]
    [todomvc.views :as views]
    [reagent.core :as r]
    [todomvc.data :as data]
    ))



(deftest render-the-main-screen
  (testing "render all sections"
    (let [component [views/screen-component data/one-todo-list-mode-no-filter]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= 1 (-> mounted (.find ".header") .-length)))
      (is (= 1 (-> mounted (.find ".main") .-length)))
      (is (= 1 (-> mounted (.find ".footer") .-length)))
      (is (= 1 (-> mounted (.find "li.todo") .-length)))
      (is (= "0 items left" (-> mounted (.find "strong") .getDOMNode .-innerText)))
      ))

  (testing "not render main section when no todos"
    (let [component [views/screen-component data/initial-model]
          shallow (->> (r/as-element component)
                       (.shallow js/enzyme))]
      (is (= 1 (-> shallow (.find ".header") .-length)))
      (is (= 0 (-> shallow (.find ".main") .-length)))
      (is (= 0 (-> shallow (.find ".footer") .-length)))
      )))



(deftest render-todos-count-component
  (testing "no item left"
    (let [component [views/todos-count-component data/one-todo-list-mode]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= "0 items left" (-> mounted
                                (.find "strong")
                                .getDOMNode
                                .-innerText)))))
  (testing "1 item left"
    (let [one (assoc-in data/one-todo-list-mode [:todos :1 :done?] false)
          component [views/todos-count-component one]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= "1 item left" (-> mounted
                               (.find "strong")
                               .getDOMNode
                               .-innerText)))))
  (testing "2 items left"
    (let [two (assoc-in data/two-todos-list-mode [:todos :1 :done?] false)
          component [views/todos-count-component two]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= "2 items left" (-> mounted
                                (.find "strong")
                                .getDOMNode
                                .-innerText))))))



(deftest render-todos-filters-component
  (testing "render no filter"
    (let [component [views/todos-filters-component nil]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= "selected" (-> mounted (.find "#all") .getDOMNode .-className)))
      (is (= "" (-> mounted (.find "#active") .getDOMNode .-className)))
      (is (= "" (-> mounted (.find "#completed") .getDOMNode .-className)))))
  (testing "render filter active"
    (let [component [views/todos-filters-component true]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= "" (-> mounted (.find "#all") .getDOMNode .-className)))
      (is (= "selected" (-> mounted (.find "#active") .getDOMNode .-className)))
      (is (= "" (-> mounted (.find "#completed") .getDOMNode .-className)))))
  (testing "render filter completed"
    (let [component [views/todos-filters-component false]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= "" (-> mounted (.find "#all") .getDOMNode .-className)))
      (is (= "" (-> mounted (.find "#active") .getDOMNode .-className)))
      (is (= "selected" (-> mounted (.find "#completed") .getDOMNode .-className))))))



(deftest invoke-controller-filter-when-clicking-filters-in-todos-filters-component
  (let [invocations (atom [])
        component [views/todos-filters-component nil]
        mounted (->> (r/as-element component)
                     (.mount js/enzyme))]
    (with-redefs [controller/filter! #(swap! invocations conj [%])]
                 (testing "unfilter"
                   (reset! invocations [])
                   (-> mounted
                       (.find "#all")
                       (.simulate "click"))
                   (is (= [[nil]] @invocations)))

                 (testing "unfilter"
                   (reset! invocations [])
                   (-> mounted
                       (.find "#active")
                       (.simulate "click"))
                   (is (= [[true]] @invocations)))

                 (testing "unfilter"
                   (reset! invocations [])
                   (-> mounted
                       (.find "#completed")
                       (.simulate "click"))
                   (is (= [[false]] @invocations)))
                 )))



(deftest render-input-text-component
  (testing "render"
    (let [text "123"
          component [views/text-input-component text "input-class" nil nil]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]

      (is (= text (-> mounted
                       (.find "input.input-class")
                       .getDOMNode
                       .-value
                       ))))))



(deftest use-keys-on-input-text-component
  (let [invocations (atom {})
        on-change (fn [x] (swap! invocations update :on-change conj [x]))
        on-quit (fn [] (swap! invocations update :on-quit conj []))]
    (testing "write something then hitting enter"
      (reset! invocations {})
      (let [component [views/text-input-component "" nil on-change on-quit]
            mounted (->> (r/as-element component)
                         (.mount js/enzyme))]
        ;WHEN
        (-> mounted
            (.find "input")
            (.simulate "change" (clj->js {:target {:value "345"}})))
        (-> mounted
            (.find "input")
            (.simulate "keydown" (clj->js {:which views/ENTER})))

        ;THEN
        (is (= {:on-change [["345"]]} @invocations))
        (is (= "" (-> mounted
                      (.find "input")
                      .getDOMNode
                      .-value
                      )))))


    (testing "hitting enter with no text"
      (reset! invocations {})
      (let [component [views/text-input-component "" nil on-change on-quit]
            mounted (->> (r/as-element component)
                         (.mount js/enzyme))]
        ;WHEN
        (-> mounted
            (.find "input")
            (.simulate "keydown" (clj->js {:which views/ENTER})))


        (is (= {} @invocations))))

    (testing "hitting esc"
      (reset! invocations {})
      (let [
            component [views/text-input-component "" nil on-change on-quit]
            mounted (->> (r/as-element component)
                         (.mount js/enzyme))]
        ;WHEN
        (-> mounted
            (.find "input")
            (.simulate "keydown" (clj->js {:which views/ESCAPE})))


        (is (= {:on-quit [[]]} @invocations))
        (is (= "" (-> mounted
                      (.find "input")
                      .getDOMNode
                      .-value
                      )))))
    ))



(deftest render-todo-input-component
  (let [component [views/todo-input-component]
        mounted (->> (r/as-element component)
                     (.mount js/enzyme))]
    (is (= "" (-> mounted
                  (.find "input.new-todo")
                  .getDOMNode
                  .-value
                  )))))


(deftest add-a-new-todo-when-clicking-enter-or-go-to-list-on-escape
  (let [invocations (atom [])]
    (with-redefs [controller/save! #(swap! invocations conj [%])
                  controller/list! #(swap! invocations conj [])]
                 (testing "invoke save"
                   (reset! invocations [])
                   (let [component [views/todo-input-component]
                         mounted (->> (r/as-element component)
                                      (.mount js/enzyme))]

                     (-> mounted
                         (.find "input.new-todo")
                         (.simulate "change" (clj->js {:target {:value "new todo"}})))
                     (-> mounted
                         (.find "input")
                         (.simulate "keydown" (clj->js {:which views/ENTER})))

                     (is (= [["new todo"]] @invocations))
                     ))
                 (testing "invoke list on esc"
                   (reset! invocations [])
                   (let [component [views/todo-input-component]
                         mounted (->> (r/as-element component)
                                      (.mount js/enzyme))]

                     (-> mounted
                         (.find "input.new-todo")
                         (.simulate "change" (clj->js {:target {:value "new todo"}})))
                     (-> mounted
                         (.find "input")
                         (.simulate "keydown" (clj->js {:which views/ESCAPE})))

                     (is (= [[]] @invocations))
                     ))
                 )))



(deftest render-todos-list-component
  (testing "no filter "
    (let [component [views/todos-list-component data/two-todo-list-mode-no-filter]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]

      (is (= 2 (-> mounted
                   (.find "li.todo")
                   .-length
                   )))))
  (testing "filter active"
    (let [component [views/todos-list-component data/two-todos-list-mode]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]

      (is (= 1 (-> mounted
                   (.find "#label_2")
                   .-length
                   )))))
  (testing "filter compelted"
    (let [filtered (-> data/two-todos-list-mode
                       (assoc-in [:context :filter] false))
          component [views/todos-list-component filtered]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]

      (is (= 1 (-> mounted
                   (.find "#label_1")
                   .-length
                   )))
      (is (= 1 (-> mounted
                   (.find "li.completed")
                   .-length
                   )))
      ))
  )


(deftest render-todo-item-component
  (testing "normal mode"
    (let [component [views/todo-item-component data/a-second-todo false]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= (:done? data/a-second-todo) (-> mounted (.find "#checkbox_2") .getDOMNode .-checked)))
      (is (= (:text data/a-second-todo) (-> mounted (.find "#label_2") .getDOMNode .-innerHTML)))
      (is (= 0 (-> mounted (.find "li.completed") .-length)))
      ))
  (testing "completed mode"
    (let [component [views/todo-item-component data/a-todo false]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= (:done? data/a-todo) (-> mounted (.find "#checkbox_1") .getDOMNode .-checked)))
      (is (= (:text data/a-todo) (-> mounted (.find "#label_1") .getDOMNode .-innerHTML)))
      (is (= 1 (-> mounted (.find "li.completed") .-length)))
      ))
  (testing "editing mode"
    (let [component [views/todo-item-component data/a-todo true]
          mounted (->> (r/as-element component)
                       (.mount js/enzyme))]
      (is (= (:done? data/a-todo) (-> mounted (.find "#checkbox_1") .getDOMNode .-checked)))
      (is (= (:text data/a-todo) (-> mounted (.find "#label_1") .getDOMNode .-innerHTML)))
      (is (= 1 (-> mounted (.find "li.completed.editing") .-length)))
      (is (= (:text data/a-todo) (-> mounted (.find "input.edit") .getDOMNode .-value)))
      ))

  )

(deftest toggle-a-todo-item-component
  (let [invocations (atom {})]
    (with-redefs [controller/toggle! #(swap! invocations update ::controller/toggle! conj [%])
                  controller/delete! #(swap! invocations update ::controller/delete! conj [%])
                  controller/edit! #(swap! invocations update ::controller/edit! conj [%])]
                 (let [component [views/todo-item-component data/a-todo]
                       mounted (->> (r/as-element component)
                                    (.mount js/enzyme))]
                   (-> mounted
                       (.find "#checkbox_1")
                       (.simulate "change"))

                   (is (= {::controller/toggle! [[(:id data/a-todo)]]} @invocations))

                   (-> mounted
                       (.find "#button_1")
                       (.simulate "click"))

                   (is (= {::controller/toggle! [[(:id data/a-todo)]]
                           ::controller/delete! [[(:id data/a-todo)]]
                           } @invocations))

                   (-> mounted
                       (.find "#label_1")
                       (.simulate "doubleClick"))

                   (is (= {::controller/toggle! [[(:id data/a-todo)]]
                           ::controller/delete! [[(:id data/a-todo)]]
                           ::controller/edit! [[(:id data/a-todo)]]
                           } @invocations))
                   ))))






