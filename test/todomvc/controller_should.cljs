(ns todomvc.controller-should
  (:require
    [cljs.test :refer-macros [deftest is testing use-fixtures]]
    [todomvc.controller :as controller]
    [todomvc.model :refer [model ADD EDIT LIST]]
    [tech.danbunea.pi :refer [commit!]]
    [todomvc.data :as data]
    ))

(use-fixtures :each {:before (fn [] (reset! model {:context {:status LIST}}))})




(defn status [state]
  (-> state
      :context
      :status))

(deftest initialize-in-list-mode
  (is (= data/initial-model (controller/init!))))



(deftest toggle-filters
  (testing "set a filter"
    (let [after (controller/filter! true)]
      (is (= {:status LIST
              :filter true}
             (:context after))
          ))
    (let [after (controller/filter! false)]
      (is (= {:status LIST
              :filter false}
             (:context after))
          )))
  (testing "remove all filters"
    (let [after (controller/filter! nil)]
      (is (= {:status LIST} (:context after)))))
  )



(deftest set-add-mode
  (is (= ADD (status (controller/add!)))))

(deftest set-list-mode
  (is (= LIST (status (controller/list!)))))



(deftest set-one-for-edit
  ;GIVEN
  (commit! data/one-todo-list-mode model)
  ;WHEN/THEN
  (is (= data/one-todo-edit-mode (controller/edit! 1))))



(deftest save-a-new-todo
  (let [after (controller/save! "todo")
        todo-id (-> after
                    :todos
                    keys
                    first)
        todo (-> after
                 :todos
                 todo-id)]
    (is (= {:text "todo" :done? false} (select-keys todo [:text :done?])))
    (is (int? (:id todo)))
    (is (= LIST (status after)))))




(deftest save-a-changed-todo
  ;GIVEN
  (commit! data/one-todo-edit-mode
           model)
  ;WHEN
  (let [expected (-> data/one-todo-list-mode
                     (assoc-in [:todos :1 :text] "todo updated"))
        after (controller/save! 1 "todo updated" true 123)]
    ;THEN
    (is (= expected after))))



(deftest delete
  ;GIVEN
  (commit! data/one-todo-list-mode model)
  ;WHEN
  (let [expected (assoc data/one-todo-list-mode :todos {})
        after (controller/delete! 1)]
    ;THEN
    (is (= expected after))))


(deftest check&unckeck
  (let [initial (assoc-in data/one-todo-list-mode [:todos :1 :done?] false)]
    ;GIVEN
    (commit! initial model)
    ;WHEN done
    (is (= data/one-todo-list-mode (controller/toggle! 1)))
    ;WHEN undone
    (is (= initial (controller/toggle! 1)))
    ))