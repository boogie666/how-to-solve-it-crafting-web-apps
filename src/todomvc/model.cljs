(ns todomvc.model
  (:require
    [reagent.core :as r :refer [atom]]
    [cljs.spec.alpha :as s]))

(def ADD "add")
(def LIST "list")
(def EDIT "edit")

(def initial-model {})

(defonce model (r/atom initial-model))

(defn ^:export jsmodel []
  (clj->js @model))



;SPEC
(s/def ::status #{"list" "edit" "add"})
(s/def ::filter #(or (false? %) (true? %)))
(s/def ::filter-with-nil #(or (nil? %) (false? %) (true? %)))
(s/def ::selected-id nat-int?)
(s/def ::context (s/keys :req-un [::status] :opt-un [::filter-id ::selected-id]))


(s/def ::id nat-int?)
(s/def ::text string?)
(s/def ::done? boolean?)
(s/def ::time number?)
(s/def ::todo (s/keys :req-un [::id ::text ::done? ::time]))
(s/def ::todos (s/map-of keyword? ::todo))

(defn edit-mode-has-a-valid-selected-id? [state]
  (if (= "edit" (get-in state [:context :status]))
    (some (into #{} (map :id (vals (:todos state)))) [(get-in state [:context :selected-id])])
    true
    ))

(s/def ::model (s/and
                 (s/keys :req-un [::context] :opt-un [::todos])
                 edit-mode-has-a-valid-selected-id?
                 ))



(comment
  (s/explain  1)
  (def m {:context {:status      "edit"
                    :filter      false
                    :selected-id 1},
          :todos   {
                    :1 {:id 1, :text "todo 1", :done? true, :time 1}
                    :2 {:id 2, :text "todo 2", :done? false, :time 2}
                    }})

  (s/valid? ::model m)
  (s/valid? ::model {:context {:status "list"}})

  )