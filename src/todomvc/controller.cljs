(ns todomvc.controller
  (:require
    [tech.danbunea.pi :refer [commit!]]
    [todomvc.model :refer [model ADD LIST EDIT]]
    [cljs.spec.alpha :as s])
  (:require-macros
    [tech.danbunea.macros :refer [safe]]
    ))


(defn init! []
  {:post [(s/valid? :todomvc.model/model %)]}
  (safe
    (-> @model
        (assoc :todos {})
        (assoc-in [:context :status] LIST)
        (commit! model))))


(defn toggle-filter [state criteria]
  (if (not (nil? criteria))
    (assoc-in state [:context :filter] criteria)
    (assoc state :context {:status (get-in state [:context :status])})))


(defn filter! [criteria]
  {:pre [(s/valid? :todomvc.model/model @model)  (s/valid? :todomvc.model/filter-with-nil criteria)]
   :post [(s/valid? :todomvc.model/model %)]}
  (safe
    (-> @model
        (assoc-in [:context :status] LIST)
        (toggle-filter criteria)
        (commit! model))))


(defn set-status! [status]
  {:pre [(s/valid? :todomvc.model/model @model) (s/valid? :todomvc.model/status status)]
   :post [(s/valid? :todomvc.model/model %)]}
  (safe
    (-> @model
        (assoc-in [:context :status] status)
        (commit! model))))

(defn add! []
  (set-status! ADD))

(defn list! []
  (set-status! LIST))


(defn keywordize [id]
  (keyword (str id)))


(defn edit! [id]
  {:pre [(s/valid? :todomvc.model/model @model) (s/valid? :todomvc.model/id id)]
   :post [(s/valid? :todomvc.model/model %)]}
  (when (get-in @model [:todos (keywordize id)])
    (safe
      (-> @model
          (assoc-in [:context :status] EDIT)
          (assoc-in [:context :selected-id] id)
          (commit! model)))))


(defn save!
  ([todo]
   (save! (rand-int 1000) todo false (.getTime (js/Date.))))
  ([id text done? time]
   {:pre [(s/valid? :todomvc.model/model @model)
          (s/valid? :todomvc.model/id id)
          (s/valid? :todomvc.model/text text)
          (s/valid? :todomvc.model/done? done?)
          (s/valid? :todomvc.model/time time)
          ]
    :post [(s/valid? :todomvc.model/model %)]}
   (let [context (-> (:context @model)
                     (assoc :status LIST)
                     (dissoc :selected-id))
         todo {:id    id
               :text  text
               :done? done?
               :time  time}]
     (safe
       (-> @model
           (assoc :context context)
           (assoc-in [:todos (keywordize id)] todo)
           (commit! model))))))


(defn delete! [id]
  {:pre [(s/valid? :todomvc.model/model @model) (s/valid? :todomvc.model/id id)]
   :post [(s/valid? :todomvc.model/model %)]}
  (let [todos (dissoc (:todos @model) (keywordize id))]
    (safe
      (-> @model
          (assoc-in [:context :status] LIST)
          (assoc :todos todos)
          (commit! model)))))



(defn toggle! [id]
  {:pre [(s/valid? :todomvc.model/model @model)  (s/valid? :todomvc.model/id id)]
   :post [(s/valid? :todomvc.model/model %)]}
  (let [path [:todos (keywordize id) :done?]
        done? (not (get-in @model path))]
    (safe
      (-> @model
          (assoc-in [:context :status] LIST)
          (assoc-in path done?)
          (commit! model)))))







