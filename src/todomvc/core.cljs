(ns ^:figwheel-hooks todomvc.core
  (:require
    [goog.dom :as gdom]
    [reagent.core :as reagent :refer [atom]]
    [todomvc.controller :as controller]
    [todomvc.model :refer [model]]
    [todomvc.views :as views :refer [screen-component]]
    [cljs.spec.alpha :as s]
    ))

(println "Reloading src/todomvc/core.cljs.")



;; define your app data so that it doesn't get over-written on reload

;initialize once
(defonce init
         (do
           (controller/init!)
           ))

(defn get-app-element []
  (gdom/getElement "app"))

(defn app []
  {:pre [(s/valid? :todomvc.model/model @model)]}
  [screen-component @model]
  )

(defn mount [el]
  (reagent/render-component [app] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  )

