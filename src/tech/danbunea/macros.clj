(ns tech.danbunea.macros)


(defmacro safe [& body]
  `(try ~@body
        (catch :default e#
          (tech.danbunea.pi/add-error e#))))





